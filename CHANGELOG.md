# CHANGELOG

<!--- next entry here -->

## 0.1.0
2021-07-20

### Features

- print version number (c92a339789761d4c57f4e8e5d95b9363c94698ae)

### Fixes

- use .next-version for versioning (6ea95f20614e58e0149b613592f594e6a6d61e87)