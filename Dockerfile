FROM alpine:3.14

ADD .next-version /.version

CMD ["cat", "/.version"]
